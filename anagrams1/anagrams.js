document.getElementById("findButton").onclick = function () {
    let typedText = document.getElementById("input").value;
    getAnagramsOf(typedText);
}
function getAnagramsOf(input) {
    let anagrams = [];
    for(let i = 0; i < words.length; i ++){
        if(alphabetize(words[i]) === alphabetize(input))
            anagrams.push(words[i]);
    }
    output(anagrams);
    return anagrams;
}
function output(array){
    let outputString = "";
    for(let i = 0; i < array.length; i ++){
        outputString += (array[i] + " ");
    }
    let node = document.createElement("LI");
    let textContent = document.createTextNode(outputString);
    node.appendChild(textContent);
    document.getElementById("Display").prepend(node);
}
function alphabetize(a) {
    return a.toLowerCase().split("").sort().join("").trim();
}